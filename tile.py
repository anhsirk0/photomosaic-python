class Tile:
    """
    Create a Tile object to store path and avg_rgb of an image
    @param path as str
    @param avg_rgb as arr
    """
    def __init__(self, img, avg_rgb):
        self.img = img
        self.avg_rgb = avg_rgb

    def __str__(self):
        return f"'{self.path}' - {self.avg_rgb}"
