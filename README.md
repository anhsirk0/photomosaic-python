# Photomosaic Creator

## Screenshots
### Input image
![fry.jpg](https://codeberg.org/anhsirk0/photomosaic-python/raw/branch/main/images/fry.jpg)

### Output image
![output.png](https://codeberg.org/anhsirk0/photomosaic-python/raw/branch/main/images/output.png)

### Part of input image
![fry_part.jpg](https://codeberg.org/anhsirk0/photomosaic-python/raw/branch/main/images/fry_part.jpg)

### Part of output image
![output_part.png](https://codeberg.org/anhsirk0/photomosaic-python/raw/branch/main/images/output_part.png)

## Usage
```shell
python3 main.py -i input.png -o output.png -t ./tiles/
```
> -i for input image

> -o to specify output image name

> -t to specify the images directory from which tiles would be created

> more the number of tiles the better the image will be 

```shell
python3 main.py --help
```
